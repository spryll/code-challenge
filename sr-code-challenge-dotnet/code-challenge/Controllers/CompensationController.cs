﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using challenge.Services;
using challenge.Models;
using challenge.Data;

namespace challenge.Controllers
{
    [Route("api/compensation")]
    public class CompensationController : Controller
    {
        private readonly CompensationContext _compensationContext;
        private readonly EmployeeContext _employeeContext;

        public CompensationController(CompensationContext compensationContext, EmployeeContext employeeContext)
        {
            _compensationContext = compensationContext;
            _employeeContext = employeeContext;
        }

        [HttpGet]
        public List<Compensation> GetAll()
        {
            return _compensationContext.Compensations.ToList();
        }

        [HttpGet("{id}", Name = "getCompensationByEmployeeId")]
        public IActionResult GetById(String id)
        {
            var item = _compensationContext.Compensations.SingleOrDefault(c => c.employee.EmployeeId == id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
        [HttpPost("{id}", Name = "insertCompensationByEmployeeId")]
        public IActionResult CreateCompensation(String id, [FromBody] Compensation compensation)
        {
            if (compensation == null)
            {
                return BadRequest();
            }
            
            compensation.employee = _employeeContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
            _compensationContext.Compensations.Add(compensation);
            _compensationContext.SaveChanges();

            return CreatedAtRoute("getCompensationByEmployeeId", new { id = compensation.employee.EmployeeId }, compensation);
            
        }
    }
}
