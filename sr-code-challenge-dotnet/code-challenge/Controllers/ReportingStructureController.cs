﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using challenge.Services;
using challenge.Models;
using challenge.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Specialized;

namespace challenge.Controllers
{
    [Route("api/reportingstructure")]
    public class ReportingStructureController : Controller
    {
        private readonly EmployeeContext _context;

        public ReportingStructureController(EmployeeContext context)
        {
            _context = context;
        }
        
        [HttpGet("{id}", Name = "getReportingStructureById")]
        public async Task<IActionResult> GetReportingStructureByIdAsync(String id)
        {
            StringCollection visitedEmployees = new StringCollection();
            ReportingStructure reportingStructure = new ReportingStructure();
            int numDirReps = getNumberDirectReports(id, visitedEmployees, numDirReps = 0);

            reportingStructure.numberOfReports = numDirReps;
            reportingStructure.employee = _context.Employees.SingleOrDefault(e => e.EmployeeId == id);

            return Ok(reportingStructure);
        }
        private int getNumberDirectReports(String employeeId, StringCollection visitedEmployees, int numDirReps)
        {
            Employee employee = _context.Employees.Include(u => u.DirectReports).SingleOrDefault(e => e.EmployeeId == employeeId);
            visitedEmployees.Add(employee.EmployeeId);

            for (int i = 0; i < employee.DirectReports.Count(); i++)
            {
                if (!visitedEmployees.Contains(employee.DirectReports[i].EmployeeId))
                {
                    numDirReps++;
                    numDirReps = getNumberDirectReports(employee.DirectReports[i].EmployeeId, visitedEmployees, numDirReps);
                }               
            }
            return numDirReps;
        }

    }
}
